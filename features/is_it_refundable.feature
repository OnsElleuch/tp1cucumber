Feature: Is it refundable ? 
    The client wants to know if the item they bought is refundable

    Scenario: The item is or is not refundable 
        Given the "<state>" of the item
        When I ask whether it's refundable or not
        Then I should be told "<answer>"
    
    Examples:
    | state                        | answer      |
    | Used                         | Nope        |
    | Damaged                      | Nope        |
    | Bought in more than a month  | Nope        |
    | Without reciept              | Nope        |
    | Bought in less than a month  | Refundable  |
    | Good as New                  | Refundable  |
    | With reciept                 | Refundable  |

