const assert = require('assert');
const { Given, When, Then } = require('@cucumber/cucumber');

function isItRefundable(state) {
   
    switch(state) {
        case 'Used':
          return "Nope";
        case 'Damaged':
           return "Nope";
        case 'Bought in more than a month':
           return "Nope";
        case 'Without reciept':
            return "Nope";
        default:
          return "Refundable"
      }
  }
  
Given('the {string} of the item', function (givenState) {
    this.state = givenState;
  });



When('I ask whether it\'s refundable or not', function () {
    this.actualAnswer = isItRefundable(this.state);
  });

Then('I should be told {string}', function (expectedAnswer) {
    assert.equal(this.actualAnswer, expectedAnswer);
  });
 